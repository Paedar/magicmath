package mtg.math;

import java.util.Collection;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Sim {
    private final int numBoosters;
    private final int commonsPerBooster;
    private final int uniqueCommons;
    private final Random rng;

    public Sim(int numBoosters, int commonsPerBooster, int uniqueCommons) {
        this.numBoosters = numBoosters;
        this.commonsPerBooster = commonsPerBooster;
        this.uniqueCommons = uniqueCommons;
        this.rng = new Random();
    }

    public int doSim() {
        return Stream.generate(this::generateBooster)
              .limit(numBoosters)
              .parallel()
              .flatMap(Collection::stream)
              .collect(Collectors.toSet())
              .size();
    }

    private Set<Integer> generateBooster() {
        var booster = new HashSet<Integer>();
        while(booster.size() < commonsPerBooster) {
            var common = generateRandomInclusiveInteger(uniqueCommons);
            booster.add(common);
        }
        return booster;
    }

    private int generateRandomInclusiveInteger(int upperExclusiveBound) {
        return rng.nextInt(upperExclusiveBound);
    }
}
