package mtg.math;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        long numSims = 1000000;

        int numBoosters = 12;
        int commonsPerBooster = 4;
        int uniqueCommons = 117;

        var results = Stream.generate(() -> new Sim(numBoosters, commonsPerBooster, uniqueCommons))
                            .limit(numSims)
                            .parallel()
                            .map(Sim::doSim)
                            .collect(Collectors.toList());

        var countByOccurence = results.stream()
                            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        var summaryStatistics = results.stream()
                                       .mapToInt(it -> it)
                                       .summaryStatistics();

        System.out.println(summaryStatistics);
        countByOccurence.keySet()
                        .stream()
                        .sorted()
                        .forEach(it -> System.out.println(it + "=" + countByOccurence.get(it) + " (" + (countByOccurence.get(it)/(float)numSims) + ")"));
    }

}
